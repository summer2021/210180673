# ACL静态库编译说明

为什么选它？其一是因为ACL是ARM NN的底层计算库，但NENO加速库算子是一堆底层汇编，ACL可以理解为是中间的一个抽象层，封装NENO提供C++的接口和example，支持linux、安卓、裸机，划重点！支持裸机！

看起来非常的便捷，实际上遇到了很多问题...

## 在linux上试用
首先本着先试用的原则，我在台式机linux上交叉编译后在运行linux系统的树莓派上测试，这里就有坑！官方使用的scons工具链，后面跟了一堆参数（十几个到二十几个不等），首先是工具链版本必须要6.3以上的gcc/g++，然后官方并没有一个写的很明白的参数手册，只有寥寥几个例子（其中参数引用还有错），摸索尝试了一番，使用ACL在树莓派上成功跑了一个alexnet分类模型。

## 裸机编译
下一步是编译裸机模式下的库，不出意外的是按照官方流程编译又失败了（我感觉ARM就没把裸机当回事，可能没人会想着在A核上搞裸机），然后跟其中的一位维护者往来几封email，外加试了一堆堆参数，成功编译了裸机模式的ACL静态库！

修改SConstruct的第243行的编译器选项为：***arm-none-eabi-***

并且将编译器路径添加到/etc/profile结尾处

执行：

***scons arch=arm64-v8a os=bare_metal build_dir=bm -j8 Werror=1 opencl=0 neon=1 debug=0 asserts=1 standalone=1 validation_tests=1 examples=1 openmp=0 cppthreads=0***

## 示例编译
由于官方连个API手册都没有，以至于我需要通过去学习它的示例，才知道该如何写自己的代码。但是呢，它的example=1选项根本不支持arm-v7a（这也是一个bug，维护者还感谢我的发现，说以后这里要禁止v7a编译示例，我晕）。所以没办法了，我决定找一个最简单的NENO_CNN.cpp，把它作为TencentOS-Tiny的一个驱动来编译，缺什么我就补什么文件进来。这是一个重要的基础，只要能够成功编译一个示例，那么就说明我掌握了正确使用NENO加速算子的方法，那么就可以去写ACL算子和后端推理框架的兼容层了，这是一件很有意义的事情！

想想很美好，但在3中还未成功编译这个最简单的示例，因为ACL库实在太大了依赖众多，上百个错误得一个一个解决，外加我对makefile和scons其实不是那么熟悉，也是边学边弄，统一操作系统的编译器和ACL的编译器也是个难题，试了很多目前选用的是，主要ACL库太吃编译器了。

后面我换了一种新的解决思路，在2的基础之上链接静态.a库，然后使用gcc工具链直接编译NENO_CPP.cpp文件：

***arm-none-eabi-g++ examples/neon_convolution.cpp utils/Utils.cpp -I. -Iinclude -std=c++14 -larm_compute-static -larm_compute_core-static -Lbuild/bm -L. build/bm/libarm_compute-static.a build/bm/libarm_compute_core-static.a***

最终可以生成一个可执行文件a.out

## 合并至RT-Thread
这一部分主要就是编译器的统一，测试了十几种编译器，目前能同时编译RTT和ACL库的只有***gcc-arm-none-eabi-7-2017-q4-major*** ，版本太高太低都不行，但是这款编译器在执行3编译示例时会出现std::mutex的问题，这个已经向官方的github提问，但暂时还没有回复。

补充：经过摸索，可以成功用arm-none-eabi-g++编译ACL库，但必须做如下修改：

1.去除arm_compute/runtime/NEON/NEFunctions.h中的91行 NEHarrisCorners.h

2.重新编译ACL库

3.修改编译指令为：

***arm-none-eabi-g++ examples/neon_convolution.cpp utils/Utils.cpp -I. -Iinclude -std=c++14 -larm_compute-static -larm_compute_core-static -Lbuild/bm -L. build/bm/libarm_compute-static.a build/bm/libarm_compute_core-static.a -static-libstdc++ --specs=nosys.specs -march=armv7-a -mfpu=neon -mfloat-abi=hard***