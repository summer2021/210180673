# imx6ull bsp使用说明

## 使用方法

打开 `Env` 工具，使用 `scons` 命令生成 `rtthread.bin` 文件

## 程序烧写
### 烧写正点原子官方uboot
- chmod 777 imxdownload /* 给 `imxdownload` 文件执行权限，只需执行一次即可 */
- ./imxdownload uboot.bin /dev/sdb /* 下载 bin 文件到 SD 卡 */

### RT-Thread在imx6ull上的启动方式修改

uboot烧录操作如下：
1. 修改arch/arm/arm-v7a/lds/link.lds中的链接地址为0x90000000(也可以不改)
2. scons -c 后再scons重新编译
3. 串口助手设置波特率为115200并启动开发板
4. 在uboot等待跳转倒计时按下enter，输入：setenv bootcmd "dhcp 0x90000000 192.168.1.101:rtthread.bin;dcache flush;go 0x90000000"，并输入saveenv保存，其中192.168.1.101是电脑主机的ip地址，这里的go地址一定要和链接脚本里的地址一致

5. 在电脑主机（windows）上安装Tftpd64软件，在settings里面设置默认tftp文件路径为rt-thread.bin工程路径，ip地址修改为本机ip，注意此软件必须要打开才可以进行tftp功能，ip地址也可以使用默认的127.0.0.1，但速度会慢很多，最好修改为本机ip

6. 在另一台电脑主机（linux）上配置Tftp服务器，我的做法是新建/etc/xinted.d/tftp文件并且输入配置信息（网上参考很多），然后重新启动tftp服务，注意要修改/etc/default/tftpd-hpa里面的烧录目标文件夹路径，并且chmod 777 Tftpboot 提升权限，**工具链仅支持*arm-none-eabi-gcc*系列**（经过测试5.4、6.2、7.2版本可以编译并下载运行成功。但最新的10.3不能成功）。

7. 重启开发板，可以看到示例的task打印程序在imx6ull上正常运行

   ![9](./../doc_zhongqi/pic/1.png)

## 驱动支持

| **外设** | **支持情况** |        **备注**         |
|:---------|:------------:|:-----------------------:|
| UART     |     支持     | UART1(DMA 模式暂未支持) |
| GPIO     |     支持     |                         |

## 开发板支持

| **外设** | **支持情况** |        **备注**         |
|:---------|:------------:|:-----------------------:|
| 正点原子     |     支持     |                         |

## 注意事项
- 在linux下要在menuconfig中关掉libc支持，并scons -c再scons编译
- 在linux下目前仅gcc-none-eabi 5.4/6.2/7.2测试成功，arm-linux-gnueabihf-gcc编译器未能测试成功（显示指令不支持）