# SOC开源项目结项报告

本报告主要配合代码仓库，介绍整体项目的成果和待改进之处

项目文档细分为
- 《RT-AK之imx6ull》
- 《RT-AK之imx6ull快速上手》
- 《im6ull bsp使用说明》
- 《ACL静态库编译说明》
- 《中期总结报告》
  
具体文档路径见 $\downarrow$

## 仓库代码及文档说明

```shell
210180673
├── ARM_Compute_Library
│   └── ACL静态库编译说明.md
├── doc_zhongqi  # 存放中期报告
│   └── 中期总结报告.md   # 包括对摄像头和ACL库的探索开发过程  
├── imx6ull_ai_TFLite_project  # 带TFLite库的imx6ull工程（经过RTAK部署之后）
├── imx6ull_base_bsp           # imx6ull的原始bsp（已开启c++11支持，windows可用）
├── plugin_imx6ull             # 支持imx6ull的RTAK插件（仅支持tflite）
│   ├── doc
│   │   └── RT-AK之imx6ull快速上手.md   #介绍如何使用imx6ull_plugin插件
│   ├── RT-AK之imx6ull.md      # 介绍imx6ull_plugin插件的工作流程
├── tflite_micro_package       # RTAK使用的tflite micro组件（无需通过软件包） 
├── README.md                  # 项目结项文档
```

## 任务完成情况总结：

使用RTAK工具在基于RT-Thread的cortex-a上一键式部署AI模型完成推理

part1：基于tensorflow lite micro的推理后端
- [x] 实现基于imx6ull的基础bsp（windows下）
- [x] 可直接复制进bsp工程的tensorflow lite micro组件
- [x] 可实现基于tflite micro的一键式部署RTAK推理后端plugin_imx6ull

part2: 基于Arm Compute Library的推理后端
- [x] 实现基于imx6ull的基础bsp（基于linux下）
- [x] 可与bsp一同编译的Arm Compute Library组件（包括静态库和配置）
- [ ] 可实现基于ACL的一键式部署RTAK推理后端plugin_imx6ull

## 改进与完善：
- [ ] imx6ull_plugin支持对tflite格式模型的解析
- [ ] ACL库在imx6ull上的运行测试（支持neon加速）
- [ ] 合并ACL库进入imx6ull_plugin
- [ ] 增添参数选择推理框架的功能
- [ ] 摄像头驱动开发

## 补充说明：
- tflite的模型解析有k210的参考，可在后续几天内完成

- ACL库的裸机移植难度较大，经过较多时间已经探索出了使用交叉工具链成功编译CNN示例的可行方法，但与bsp混合编译时在链接仍存在问题，需继续检查编译配置
- 摄像头驱动目前没有直接的参考，经过初步摸索，目前可以检测到帧中断信号，但输出图像并不正确，需继续检测引脚对应等问题
- 第2、3项难度超出项目立题时的预期，后续继续探索

## 参考仓库：
https://github.com/tyustli/imx6ull

https://github.com/ARM-software/ComputeLibrary

https://github.com/QingChuanWS/TensorflowLiteMicro

## 鸣谢：
来自RT-Thread的李奇文、陈晨毅、杨武、熊大等几位老师对项目期间存在的疑惑进行解答

来自西电的刘恒言同学对tflite micro软件包使用方法和内部结构进行解答
